//2015110705 김민규
//본인은 이 소스파일을 복사하지 않고 직접 작성하였습니다.

#define _CRT_SECURE_NO_WARNINGS
#define REALLOC(o, p, s) {\
	free(o); \
	fprintf(stderr, "Insufficient memory"); \
	exit(EXIT_FAILURE);\
}
#define TRUE 1
#define FALSE 0

#include <stdio.h>
#include <stdlib.h>

typedef struct {
	short int vert;
	short int horiz;
	} offsets;
typedef struct {
	short int row;
	short int col;
	short int dir;
	} element;
element *stack;										//스택
int capacity = 1;									//스택 capacity
int top = -1;										//스택 top

int **maze;											//미로 배열
int **mark;											//마크 배열(지나간 길을 표시)
const offsets move[8] =								//방향별 이동 크기 배열(상수)
{
	{-1, 0}, {-1, 1}, {0 , 1}, {1, 1},
	{1, 0}, {1, -1}, {0, -1}, {-1, -1}
};		
int EXIT_ROW, EXIT_COL;

void path();
void initMaze();
void initMark();
element pop();
void push(element);
void make2dArray();
void free2dArray();

int main()
{
	FILE* pInputFile = fopen("input.txt", "r");
	int i, j;

	//파일 입력
	fscanf(pInputFile, "%d %d", &EXIT_ROW, &EXIT_COL);

	//배열 동적할당
	make2dArray();

	//배열 초기화
	initMaze();
	initMark();

	//배열에 입력
	for (i = 1; i <= EXIT_ROW; i++) {
		for (j = 1; j <= EXIT_COL; j++) {
			fscanf(pInputFile, "%d", &maze[i][j]);	
		}
	}

	//스택 동적할당
	stack = (element*)malloc(sizeof(element) * (EXIT_ROW * EXIT_COL));

	//파일 닫기
	fclose(pInputFile);

	//길찾기
	path();

	//동적할당 해제
	free2dArray();

	return 0;
}

//길찾기 함수
void path()
{
	/* output a path through the maze if such a path exists */
	int i, row, col, nextRow, nextCol, dir, found = FALSE;
	element position;
	mark[1][1] = 1; top = 0;
	stack[0].row = 1; stack[0].col = 1; stack[0].dir = 1;
	while (top > -1 && !found) {
		position = pop();
		row = position.row; col = position.col;
		dir = position.dir;
		while (dir < 8 && !found) {
			nextRow = row + move[dir].vert;
			nextCol = col + move[dir].horiz;
			if (nextRow == EXIT_ROW && nextCol == EXIT_COL)
				found = TRUE;
			else if ( !maze[nextRow][nextCol] &&
				! mark[nextRow][nextCol]) {
				mark[nextRow][nextCol] = 1;
				position.row = row; position.col = col;
				position.dir = ++dir;
				push(position);
				row = nextRow; col = nextCol; dir = 0;
			}
			else ++dir;
		}
	}

	if (found) {
		printf("The path is : \n");
		printf("row	col \n");
		for(i = 0; i <= top; i++)
			printf("%2d%5d \n", stack[i].row, stack[i].col);
		printf("%2d%5d \n", row, col);
		printf("%2d%5d\n", EXIT_ROW, EXIT_COL);
	}
	else printf("The maze does not have a path \n");
}

//initialize - maze
void initMaze()
{
	int i, j;

	for (i = 0; i <= EXIT_ROW + 1; i++) {
		maze[i][0] = 1;
		maze[i][EXIT_COL + 1] = 1;
	}

	for (i = 0; i <= EXIT_COL + 1; i++) {
		maze[0][i] = 1;
		maze[EXIT_ROW + 1][i] = 1;
	}
}

//initialize - mark
void initMark()
{
	int i, j;

	for (i = 0; i <= EXIT_ROW + 1; i++) {
		for (j = 0; j <= EXIT_COL + 1; j++) {
			mark[i][j] = 0;
		}
	}
}

//stack - pop
element pop()
{
	element _return = stack[top];
	top--;

	return _return;
}

//stack - push
void push(element value)
{
	top++;

	stack[top] = value;
}

//2차원 동적할당
void make2dArray()
{
	int i;

	maze = (int**)malloc(sizeof(int*) * (EXIT_ROW + 2));
	mark = (int**)malloc(sizeof(int*) * (EXIT_ROW + 2));

	for(i = 0; i <= EXIT_ROW + 1; i++) {
		maze[i] = (int*)malloc(sizeof(int) * (EXIT_COL + 2));
		mark[i] = (int*)malloc(sizeof(int) * (EXIT_COL + 2));
	}
}

void free2dArray()
{
	int i;

	for(i = 0; i < EXIT_ROW + 2; i++) {
		free(maze[i]);
		free(mark[i]);
	}

	free(maze);
	free(mark);
}