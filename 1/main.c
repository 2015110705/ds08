//2015110705 김민규
//본인은 이 소스파일을 복사하지 않고 직접 작성하였습니다.

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#define MAX_STACK_SIZE 80							
#define MAX_EXPR_SIZE 80							//입력 수식의 문자열 길이 최대값

typedef enum {lparen, rparen, plus, minus, 
	times, divide, mod, eos, operand} precedence;	//연산자

/*	isp and icp arrays -- index is value of precedence
	lparen, rparen, plus, minus, times, divide, mod, eos */
int isp[] = {0, 19, 12, 12, 13, 13, 13, 0};
int icp[] = {20, 19, 12, 12, 13, 13, 13, 0};

char expr[MAX_EXPR_SIZE];							//input string
precedence stack[MAX_STACK_SIZE];					//stack
int top = -1;										//stack - top

void postfix();
precedence getToken(char*, int*);
void printToken(precedence);
precedence pop();									//stack - pop
void push(precedence);								//stack - push

int main()
{
	FILE* pInputFile = fopen("input.txt", "r");

	//파일로 문자열 입력
	fgets(expr, 80, pInputFile);

	//파일 닫기
	fclose(pInputFile);

	//postFix
	postfix();

	return 0;
}

void postfix()
{
	/*	output the postfix of the expression. The expression
		string, the stack, and top are global */
	char symbol;
	precedence token;
	int n = 0;
	FILE *pOutputFile = fopen("output.txt", "w");
	top = 0;			//place eos on stack
	stack[0] = eos;
	

	for (token = getToken(&symbol, &n); token != eos; 
		token = getToken(&symbol, &n)) {
		if (token == operand) {
			pOutputFile = fopen("output.txt", "a");
			printf("%c", symbol);
			fprintf(pOutputFile, "%c", symbol);
			fclose(pOutputFile);
		}
		else if (token == rparen) {
			//unstack tokens until left parenthesis
			while (stack[top] != lparen)
				printToken(pop());
			pop();
		}
		else {
			/*	remove and print symbols whose isp is greater
				than or equal to the current token's icp */
			while (isp[stack[top]] >= icp[token])
				printToken(pop());
			push(token);
		}
	}

	while ((token = pop()) != eos) 
		printToken(token);
	printf("\n");

	pOutputFile = fopen("output.txt", "a");
	fprintf(pOutputFile, "\n");
	fclose(pOutputFile);
}

//배열에 존재하는 특정 인덱스에 있는 토큰을 가져옴
precedence getToken(char* symbol, int* index)
{
	/*	get the next token, symbol is the character
		representation, which is returned, the token is
		represented by its enumerated value, which
		is returned in the function name */

	*symbol = expr[(*index)++];
	switch(*symbol) {
	case '(' : return lparen;
	case ')' : return rparen;
	case '+' : return plus;
	case '-' : return minus;
	case '/' : return divide;
	case '*' : return times;
	case '%' : return mod;
	case '\0' : return eos;
	default : return operand;	//no error checking, default is operand
	}
}

//토큰 출력
void printToken(precedence value)
{
	FILE *pOutputFile = fopen("output.txt", "a");

	//화면 및 파일 출력
	//lparen, rparen, plus, minus, times, divide, mod, eos, operand
	switch(value) {
	case lparen : printf("(");	fprintf(pOutputFile, "("); break;
	case rparen : printf(")");	fprintf(pOutputFile, ")"); break;
	case plus : printf("+");	fprintf(pOutputFile, "+"); break;
	case minus : printf("-");	fprintf(pOutputFile, "-"); break;
	case times : printf("*");	fprintf(pOutputFile, "*"); break;
	case divide : printf("/");	fprintf(pOutputFile, "/"); break;
	case mod : printf("%%");		fprintf(pOutputFile, "%%"); break;
	default : break;
	}

	//파일 닫기
	fclose(pOutputFile);
}

precedence pop()
{
	precedence _return = stack[top];
	top--;

	return _return;
}

void push(precedence value)
{
	top++;
	stack[top] = value;
}